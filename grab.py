#!/usr/bin/env python
# encoding: utf-8


import screen, sys
if not screen.calibrate():
    sys.stderr.write("Can't calibrate the board\n")
    sys.exit(1)

x0, y0 = screen.board
xSize, ySize = screen.cellSize
xSpace, ySpace = screen.cellSpace
N = screen.N
F = screen.F

for i in range(N):
    y = y0 + i * (ySize + ySpace)
    line = []
    for j in range(N):
        x = x0 + j * (xSize + xSpace)
        a = screen.getArea((x, y), screen.pixelSize)
        line.append(screen.getPixelInfo(a))
        #print getPixelInfo(a, full=True)
    print ''.join(line)

x0, y0 = screen.forecast
xSize, ySize = screen.forecastSize
xSpace, ySpace = screen.forecastSpace
line = []
for i in range(F):
    a = screen.getArea((x0 + i * (xSize + xSpace), y0), screen.pixelSize)
    line.append(screen.getPixelInfo(a))
    #print getPixelInfo(a, full=True)
print ''.join(line)
