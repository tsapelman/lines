#!/usr/bin/env python
# encoding: utf-8

import sys, random

N, F = 9, 3     # The board size and the forecasted ball amount
C = 7           # Amount of ball colors
L = 5           # Line length to be eliminated


def pathLength(board, arr, dst):
    def processNeighbours(board, temp, arr, dst):
        sides = dict( north = (0, -1), east = (1, 0), south = (0, 1), west = (-1, 0) )
        i, j = arr
        weight = temp[i][j]
        for s in list(sides.keys()):
            di, dj = sides[s]
            ni, nj = i + di, j + dj
            if 0 <= ni < N and 0 <= nj < N and (board[ni][nj] == 0 or (ni, nj) == dst):
                if temp[ni][nj] < 0 or weight+1 < temp[ni][nj]:
                    temp[ni][nj] = temp[i][j] + 1
                    processNeighbours(board, temp, (ni, nj), dst)

    # Start here
    if arr == dst:
        return 0
    temp = [ [ -1 for j in range(N) ] for i in range(N) ]
    i, j = arr
    temp[i][j] = 0
    processNeighbours(board, temp, arr, dst)
    i, j = dst
    return temp[i][j]


def randomColor():
    return random.randint(1, C)


def randomCell(b):
    emptyCells = [ (i, j) for i in range(N) for j in range(N) if not b[i][j] ]
    return random.choice(emptyCells)


def printable(value):
    colors = [ '.', 'K', 'B', 'G', 'C', 'R', 'M', 'Y' ]
    return colors[value]


def init(b, f):
    for i in range(N):
        i, j = randomCell(b)
        b[i][j] = randomColor()
    for i in range(F):
        f[i] = randomColor()


def output(b, f, move, score):
    print
    print "Move %d, score %d" % (move, score)
    for i in range(N):
        row = []
        for j in range(N):
            row.append(printable(b[i][j]))
        print ' '.join(row)

    if not isFull(b):
        row = []
        for i in range(F):
            row.append(printable(f[i]))
        print ' '.join(row)
    else:
        print "END."


def isFull(b):
    for i in range(N):
        for j in range(N):
            if not b[i][j]:
                return False
    return True


def getMove():
    def checkInput(codes):
        if len(codes) != 2:
            return False
        if codes[0] == codes[1]:
            return False
        for code in codes:
            if not 11 <= code <= 99 or code % 10 == 0:
                return False
        return True

    def decode(code):
        return (code // 10 - 1, code % 10 - 1)

    line = sys.stdin.readline().strip()
    codes = map(int, line.split())
    if checkInput(codes):
        return (decode(codes[0]), decode(codes[1]))
    else:
        return False


def makeMove(b, arr, dst):
    if pathLength(b, arr, dst) <= 0:
        return False
    iArr, jArr = arr
    iDst, jDst = dst
    if b[iArr][jArr] and not b[iDst][jDst]:
        b[iDst][jDst] = b[iArr][jArr]
        b[iArr][jArr] = 0
        return True
    else:
        return False


def eliminateLines(b):
    def findLinesForCell(b, i, j):
        DIRECTIONS = dict( east = (0, 1), eastsouth = (1, 1), south = (1, 0), southwest = (1, -1) )
        color = b[i][j]
        if not color:
            return False
        l = []
        for d in list(DIRECTIONS.keys()):
            di, dj = DIRECTIONS[d]
            for z in range(1, L):
                iTemp, jTemp = i + di * z, j + dj * z
                if iTemp not in range(N) or jTemp not in range(N) or b[iTemp][jTemp] != color:
                    break
            else:
                l.append(((i, j), DIRECTIONS[d]))
        return l

    lines = []
    for i in range(N):
        for j in range(N):
            res = findLinesForCell(b, i, j)
            if res:
                lines.extend(res)
    count = 0
    for l in lines:
        cell, d = l
        i, j = cell
        di, dj = d
        for z in range(L):
            if b[i + z * di][j + z * dj]:
                b[i + z * di][j + z * dj] = 0
                count += 2
    return count


# Start here
board = [ [ 0 for j in range(N) ] for i in range(N) ]
forecast = [ 0 for i in range(F) ]

init(board, forecast)
move = 1
score = 0
output(board, forecast, move, score)
while not isFull(board):
    res = getMove()
    if res:
        arr, dst = res
        res = makeMove(board, arr, dst)
    if not res:
        sys.stderr.write("Invalid move\n")
        sys.exit(1)
    lines = eliminateLines(board)
    score += lines
    if not lines:
        try:
            for f in forecast:
                i, j = randomCell(board)
                board[i][j] = f
                score += eliminateLines(board)
            for i in range(F):
                forecast[i] = randomColor()
        except IndexError:
            pass
    move += 1
    output(board, forecast, move, score)
