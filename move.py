#!/usr/bin/env python
# encoding: utf-8


def decodeCell(code):
    i, j = code // 10, code % 10
    if not 11 <= code <= 99 or j == 0:
        raise Exception("Can't decode %d" % code)
    return i, j

def convertToPixel(cell):
    i, j = cell
    x0, y0 = screen.board
    xSize, ySize = screen.cellSize
    xSpace, ySpace = screen.cellSpace
    x = x0 + (j - 1) * (xSize + xSpace)
    y = y0 + (i - 1) * (ySize + ySpace)
    return x, y


import sys, autopy, time, re, screen

if len(sys.argv) != 3:
    sys.stderr.write("Require 2 arguments (from-cell, to-cell)\n")
    sys.exit(1)

reArg = re.compile('^[1-9][1-9]$')
if not reArg.match(sys.argv[1]) or not reArg.match(sys.argv[2]):
    sys.stderr.write("Argument invalid\n")
    sys.exit(1)

if not screen.calibrate():
    sys.stderr.write("Can't calibrate the board\n")
    sys.exit(1)

cellFrom = decodeCell(int(sys.argv[1]))
cellTo = decodeCell(int(sys.argv[2]))

if cellFrom != cellTo:
    # Move a ball from cellFrom to cellTo cell
    x, y = convertToPixel(cellFrom)
    autopy.mouse.smooth_move(x, y)
    time.sleep(0.5)
    autopy.mouse.click()
    time.sleep(0.5)

    x, y = convertToPixel(cellTo)
    autopy.mouse.smooth_move(x, y)
    time.sleep(0.5)
    autopy.mouse.click()

else:
    # Show given cell in the separate window
    from Xlib import display, X
    from PIL import Image
    disp = display.Display()
    root = disp.screen().root
    xSize, ySize = screen.cellSize
    x, y = convertToPixel(cellFrom)
    x -= xSize // 2
    y -= ySize // 2
    raw = root.get_image(x, y, xSize, ySize, X.ZPixmap, 0xFFFFFFFF)
    Image.frombytes("RGB", screen.cellSize, raw.data, "raw", "BGRX").show()
