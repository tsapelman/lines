#!/usr/bin/env python
# encoding: utf-8


# Init display
from Xlib import display, X
disp = display.Display()
root = disp.screen().root

# Board data
N, F = 9, 3

# Screen data
_borderColor = (0xC0, 0xC0, 0xC0)
_defaultBoard = (527, 246)
_defaultCellSize = (60, 60)
board = (0, 0)
cellSize = (0, 0)
cellSpace = (1, 1)

forecast = (260, 278)
forecastSpace = (11, 0)
forecastSize = (59, 59)

pixelSize = (1, 1)

def isEmptyColor(r, g, b):
    return r == 0x40 and g == 0x80 and b == 0x40

def isBorderColor(r, g, b):
    rR, rG, rB = _borderColor
    return r == rR and g == rG and b == rB

def color(r, g, b):
    colors = [ 'K', 'B', 'G', 'C', 'R', 'M', 'Y', '.' ]
    if isEmptyColor(r, g, b):
        return colors[-1]
    d = 0xA0
    return colors[ (r // d) * 4 + (g // d) * 2 + (b // d) ]
    
def getPixelData(p):
    return ord(p[2]), ord(p[1]), ord(p[0])

def getPixelInfo(p, full=False):
    r, g, b = getPixelData(p)
    if full:
        return "(%02X:%02X:%02X)" % (r, g, b)
    return color(r, g, b)

def getArea(base, size):
    x, y = base
    xSize, ySize = size
    raw = root.get_image(x, y, xSize, ySize, X.ZPixmap, 0xFFFFFFFF)
    return raw.data



def calibrate():
    def isBorder(x, y):
        a = getArea((x, y), pixelSize)
        r, g, b = getPixelData(a)
        return isBorderColor(r, g, b)

    global cellSize
    global board
    xSize, ySize = _defaultCellSize
    xSpace, ySpace = cellSpace
    xBoard, yBoard = _defaultBoard
    x0, y0 = xBoard, yBoard
    dx, dy = 0, 0
    for d in range(xSize):
        dx += 1
        if isBorder(x0-dx, y0):
            break;
    else:
        return False

    for d in range(ySize):
        dy += 1
        if isBorder(x0, y0-dy):
            break;
    else:
        return False

    x0 -= dx
    y0 -= dy

    dx, dy = 0, 0
    while isBorder(x0 + dx, y0):
        dx += 1
        if dx > xSize * N * 2:
            return False
    while isBorder(x0, y0 + dy):
        dy += 1
        if dy > ySize * N * 2:
            return False
    if dx < N * 2 or dy < N * 2:
        return False

    xSize = (dx + N // 2) // N - xSpace
    ySize = (dy + N // 2) // N - ySpace
    cellSize = (xSize, ySize)
    x0 += xSize // 2
    y0 += ySize // 2
    board = (x0, y0)
    #print "Board: %d, %d" % (x0, y0)
    #print "CellSize: %dx%d" % (xSize, ySize)
    #print "dx:dy = %d:%d" % (dx, dy)
    return True
