#!/usr/bin/env python
# encoding: utf-8

import sys

N, F = 9, 3    # The board size and the forecasted ball amount

connected_4 = list()    # Precalculated list of 4-connected neighbours for each board cell
connected_8 = list()    # Precalculated list of 8-connected neighbours for each board cell


# Encodes color name into integer value. Returns the value
def encodeField(ch):
    FIELDS = { '.': 0, 'K': 1, 'B': 2, 'G': 3, 'C': 4, 'R': 5, 'M': 6, 'Y': 7 }
    return FIELDS[ch]


# Since many neighbours of the board cells is out of board it is a good idea
# to precalculate all real 4-connected and 8-connected neighbours for each cell
# for various purposes.
#
# Fills connected_4 and connected_8 data collections. Returns nothing.
def initConnectedLists():
    global connected_4, connected_8
    connected_4 = [ [ list() for j in range(N) ] for i in range(N) ]
    connected_8 = [ [ list() for j in range(N) ] for i in range(N) ]
    for i in range(N):
        for j in range(N):
            for s in range(4):
                # Split s to two bits
                s1, s0 = s//2, s%2

                # Map s1, s0 value to (north, east, south, west) set
                di = dj = s1 * 2 - 1
                di *= (1 - s0)
                dj *= s0
                if 0 <= i + di < N and 0 <= j+ dj < N:
                    connected_4[i][j].append((di, dj))
                    connected_8[i][j].append((di, dj))

                # Map s1, s0 value to (northwest, northeast, southeast, southwest) set
                di = s1 * 2 - 1
                dj = s0 * 2 - 1
                if 0 <= i + di < N and 0 <= j+ dj < N:
                    connected_8[i][j].append((di, dj))


# Calculates the taxicab distance on given 'board' from 'arr' (arrival) cell to 'dst' (destination) one.
# If the map for the arrival cell hits the cache skip building map stage and simply gets the distance.
# If cache miss happen builds the map and save it in the cache. Then gets the distance.
# If True passed as 'invalidate' argument value the cache will be unconditionally invalidated
# before any action occures.
# Returns the distance.
def pathLength(board, arr, dst, invalidate=False):
    # Finds current estimations on given 'board' for 4-connected neighbours of the 'cell'.
    # Then calls itself recursively for each cell obtained currently
    # As a result the first function entry builds the taxicab geometry map for the whole board.
    # Returns nothing.
    def processNeighbours(board, cell):
        i, j = cell
        weight = pathLength.pathMap[i][j]
        for s in connected_4[i][j]:
            di, dj = s
            ni, nj = i + di, j + dj
            if board[ni][nj] == 0:
                if pathLength.pathMap[ni][nj] < 0 or weight+1 < pathLength.pathMap[ni][nj]:
                    pathLength.pathMap[ni][nj] = pathLength.pathMap[i][j] + 1
                    processNeighbours(board, (ni, nj))

    # pathLength() function entry
    if invalidate:
        # The cache invalidation is required
        pathLength.pathMap = pathLength.mapArrival = False

    if arr == dst:
        # This dummy case doesn't require any calculation. Just return the result
        return 0

    if not pathLength.mapArrival or arr != pathLength.mapArrival or not pathLength.pathMap:
        # Cache miss happens. Rebuild the map
        pathLength.pathMap = [ [ -1 for j in range(N) ] for i in range(N) ]
        i, j = arr
        pathLength.pathMap[i][j] = 0
        pathLength.mapArrival = arr
        processNeighbours(board, arr)

    # Return the distance for the destination cell
    i, j = dst
    return pathLength.pathMap[i][j]

# Internal data of pathLength() function to cache recently built map
pathLength.pathMap = False
pathLength.mapArrival = False


def positionValue(brd):
    value = 0
    for i in range(N):
        for j in range(N):
            if brd[i][j] == 0:
                cnt = 0
                for s in connected_4[i][j]:
                    di, dj = s
                    iTemp, jTemp = i + di, j + dj
                    if brd[iTemp][jTemp] == 0:
                        cnt += 1
                value += 2 ** cnt
    return value


def findLinesForCell(b, i, j):
    DIRECTIONS = dict( east = (0, 1), eastsouth = (1, 1), south = (1, 0), southwest = (1, -1) )
    color = b[i][j]
    if not color:
        return False
    toCleanse = []
    for d in list(DIRECTIONS.keys()):
        di, dj = DIRECTIONS[d]
        l = []
        z = 1
        iTemp, jTemp = i, j
        while True:
            iTemp += di
            jTemp += dj
            if iTemp in range(N) and jTemp in range(N) and b[iTemp][jTemp] == color:
                l.append((iTemp, jTemp))
                z += 1
            else:
                break
        iTemp, jTemp = i, j
        while True:
            iTemp -= di
            jTemp -= dj
            if iTemp in range(N) and jTemp in range(N) and b[iTemp][jTemp] == color:
                l.append((iTemp, jTemp))
                z += 1
            else:
                break
        if z >= 5:
            l.append((i, j))
            toCleanse += l
    return toCleanse


def findTheBestMove(board):
    theBestValue = 0
    arr = (0, 0)
    dst = (0, 0)
    for row in range(N):
        for col in range(N):
            if board[row][col]:
                color = board[row][col]
                board[row][col] = 0
                for i in range(N):
                    for j in range(N):
                        if not board[i][j] and pathLength(board, (row, col), (i, j)) > 0:
                            board[i][j] = color
                            toCleanse = findLinesForCell(board, i, j)
                            for c in toCleanse:
                                iTemp, jTemp = c
                                board[iTemp][jTemp] = 0
                            value = positionValue(board)
                            if value > theBestValue:
                                theBestValue = value
                                arr = (row + 1, col + 1)
                                dst = (i + 1, j + 1)
                            for c in toCleanse:
                                iTemp, jTemp = c
                                board[iTemp][jTemp] = color
                            board[i][j] = 0
                board[row][col] = color
    return theBestValue, arr, dst


board = []
for i in range(N):
    line = sys.stdin.readline().strip()
    if len(line) != N:
        sys.stderr.write("Invalid board line\n")
        sys.exit(1)
    row = []
    for j in range(N):
        row.append(encodeField(line[j]))
    board.append(row)

forecast = []
line = sys.stdin.readline().strip()
if len(line) != F:
    sys.stderr.write("Invalid forecast line\n")
    sys.exit(1)
for i in range(F):
    forecast.append(encodeField(line[i]))

print "Board:"
for i in range(N):
    print ' '.join(str(b) for b in board[i])
print
print "Forecast:"
print ' '.join(str(b) for b in forecast)

initConnectedLists()

print "Length from 11 to 99 is %d, value = %d" % (pathLength(board, (0, 0), (8, 8)), positionValue(board))

val, arr, dst = findTheBestMove(board)

print "My move (value=%d) is %d%d-%d%d" % (val, arr[0], arr[1], dst[0], dst[1])

# Iterate each cell
# Build all possible 5-length obstacle-free combinations
# Give 2**K point per a combination, where K is the amount of balls currently present in the combination
# Summarize the points as a value of the position
